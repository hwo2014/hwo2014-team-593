module.exports = function(botName, qualAI, quickAI, raceAI, client)
{

	console.log(botName, "connected.");

	// *** server/connection variables and functions
	var JSONStream = require('JSONStream')					// lets us use JSON parsers for reading and writing server messages
	,	jsonStream = client.pipe(JSONStream.parse())		// lets us read server-sent JSON
	;

	function send(json)										// lets us send JSON to the server
	{
		client.write(JSON.stringify(json));
		return client.write('\n');
	}

	function logServerMessages(type, data)
	{
		switch (type)
		{
			case 'gameStart':
				console.log('The race is on!');
				break;
			case 'gameEnd':
				console.log('The race has ended.');
				break;
			case 'crash':
				console.log("The", data.color, "car driven by", data.name, "has crashed!");
				break;
			case 'spawn':
				console.log("The", data.color, "car driven by", data.name, "is back in the race.");
				break;
			case 'lapFinished':
				console.log("The", data.car.color, "car driven by", data.car.name, "has completed lap number", data.lapTime.lap + 1, "in", data.lapTime.ticks, "ticks.");
				break;
			case 'tournamentEnd':
				console.log('The tournament has ended.');
				break;
			case 'dnf':
				console.log("The", data.color, "car driven by", data.name, "has been disqualified. Reason:", data.reason);
				break;
			case 'finish':
				console.log("The", data.color, "car driven by", data.name, "has completed the race.");
				break;
			case 'gameInit':
			case 'yourCar':
			case 'join':
			case 'joinRace':
			case 'createRace':
			case 'carPositions':
			case 'turboAvailable':
				break;
			case 'error':
				console.log("Error:", data);
				break;
		}
	}

	// *** AI-relevant variables and functions ***
	
	var AI			// the AI we're using (qualifying or race)
	,	raceData
	,	selfColor	// the color of our own car
	;

	jsonStream.on('data', function(data)
	{
		if (AI && data.msgType)
			send(AI(selfColor, raceData, data));
		var type = data.msgType, tick = data.tick, data = data.data;		// makes our messages a little easier to process

		if (selfColor == 'red')
			logServerMessages(type, data);				// logs data that is sent to all drivers - we only need one of our bots doing this

		switch (type)
		{
			case 'join':
			case 'joinRace':
			case 'createRace':
				console.log('Joined.');
				break;

			case 'yourCar':
				selfColor = data.color;
				if (selfColor === 'orange')					// orange appears to be the only color that starts with a vowel, so we check to make sure we're using "an" :)
					console.log("Team", botName, "is driving an", selfColor, "car.");
				else
					console.log("Team", botName, "is driving a", selfColor, "car.");
				break;

			case 'error':
				console.log("Error:", data);
				break;

			case 'gameInit':
				raceData = data;
				if (data.race.raceSession.durationMs)
					AI = qualAI;
				else if (data.race.raceSession.quickRace)
					AI = quickAI;
				else
					AI = raceAI;
				break;
			case 'turboAvailable':
				console.log("Turbo available!");
				break;
			case 'carPositions':
			case 'gameStart':
			case 'gameEnd':
			case 'crash':
			case 'spawn':
			case 'lapFinished':
			case 'tournamentEnd':
			case 'dnf':
			case 'finish':
				break;
			default:
				console.log(type, "message received:", data);
				break;
		}
	});

	jsonStream.on('error', function() {
		return console.log("disconnected");
	});
};