var	getLaneSwitch	= require('./common/laneSwitches.js');
var	getDistance		= require('./common/distanceTo.js');
var getSpeed 		= require('./common/speedometer.js');
var calcMaxSpeeds	= require('./common/calcMaxSpeeds.js');

var oldTick = 0, oldPos = {}, oldThrottle = 0.0, oldSpeed = 0, speedArray = []	// static variable declaration

var maxSpeeds = [];
			var minMax = 99999999999;

var maxAccel = .427;

var onSwitchTick = 0;

var stable = false;

module.exports = function(color, raceData, data)
{
	var type = data.msgType, tick = data.gameTick, data = data.data;		// makes our messages a little easier to process
	
	
	if (type == 'carPositions')
	{
		var msg = {};
		var throttle;
		var car;
		var speed;
		for (var i=0; i<data.length; ++i)
		{
			if (data[i].id.color === color)
				car = data[i];
		}

		if (tick > 0)
			speed = getSpeed(oldPos, car.piecePosition, raceData.race.track);

		if (!(tick%30))
		{
			msg.msgType = "switchLane";
			if (car.piecePosition.lane.endLaneIndex)
				msg.data = "Left";
			else
				msg.data = "Right";
		} else
		{
			msg.msgType = "throttle";
			msg.data = throttle = .35;
			if (tick < 6)
			{
				msg.data = throttle = 1.0;
			}
			
			var cocks = throttle * 10 -.01;
			if  (speed > cocks && !stable) stable = true;
			if (stable &&  (speed < cocks || speed > cocks+.02)) console.log("FUCK", speed, raceData.race.track.pieces[car.piecePosition.pieceIndex], raceData.race.track.pieces[oldPos.pieceIndex], tick);
		}

		if (true)
		{
			if (raceData.race.track.pieces[car.piecePosition.pieceIndex]['switch'] && !onSwitchTick)
			{
				onSwitchTick = tick;
			}
			if (!raceData.race.track.pieces[car.piecePosition.pieceIndex]['switch'] && onSwitchTick)
			{
				console.log(oldPos.inPieceDistance, car.piecePosition.inPieceDistance, speed, onSwitchTick, tick);
				console.log(tick-onSwitchTick, raceData.race.track.pieces[car.piecePosition.pieceIndex - 1], speed);
				console.log("   ");
				onSwitchTick = 0;
			}
		}
		oldTick = tick;
		oldPos = car.piecePosition;
		oldSpeed = speed;
		return msg;
	}


	return {msgType: "ping"};
}