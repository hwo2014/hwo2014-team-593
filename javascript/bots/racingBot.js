var	getLaneSwitch	= require('./common/laneSwitches.js');
var	getDistance		= require('./common/distanceTo.js');
var getSpeed 		= require('./common/speedometer.js');
var calcMaxSpeeds	= require('./common/calcMaxSpeeds.js');

var oldTick = 0, oldPos = {}, oldThrottle = 0.0, oldSpeed = 0, speedArray = []	// static variable declaration

var maxSpeeds = [];
			var minMax = 99999999999;

var maxAccel = .427;

module.exports = function(color, raceData, data)
{
	var type = data.msgType, tick = data.gameTick, data = data.data;		// makes our messages a little easier to process
	
	
	if (type == 'carPositions')
	{
		var msg = {};
		var throttle;
		var car;
		for (var i=0; i<data.length; ++i)
		{
			if (data[i].id.color === color)
			{
				car = data[i];
				break;
			}
		}
		
		if (tick > 0) {
			var speed = getSpeed(oldPos, car.piecePosition, raceData.race.track);
			
			//console.log(minMax);
			
			if (tick < 50)
				throttle = 1.0;
			else if (tick < 400)
				throttle = (minMax/10);
			else
				throttle = 0;

			console.log("%s %s %s %s %s", speed, "\t", speed-oldSpeed, "\t", tick);

			if (throttle != oldThrottle)
			{
				oldThrottle = throttle;
				msg.msgType = "throttle";
				msg.data = throttle;
			}
			// if we didn't have to make a throttle adjustment, we can take a look at lane changing
			// this only needs to happen once between every 2 switch pieces, but we can just spam it whenever we're not looking at throttle, which should happen often enough that we don't miss a switch
			else
			{
				msg.msgType = "switchLane";
				switch(getLaneSwitch(car.piecePosition.pieceIndex, raceData.race.track))
				{
					case -1:
						msg.data = "Left";
						break;
					case  1:
						msg.data = "Right";
						break;
					case 0:
						msg.msgType = "ping";
						break;
				}
			}
		}
		else {
			maxSpeeds = calcMaxSpeeds(maxAccel, raceData);
			msg.msgType = "throttle";
			msg.data = 1.0;
		}
		
		oldTick = tick;
		oldPos = car.piecePosition;
		oldSpeed = speed;
		return msg;
	}


	return {msgType: "ping"};
}