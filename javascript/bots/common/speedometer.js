var	getDistance		= require('./distanceTo.js');

module.exports = function(lastPos, curPos, track)
{
	return getDistance(lastPos, curPos, track);
};