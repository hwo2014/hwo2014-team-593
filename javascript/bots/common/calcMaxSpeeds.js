module.exports = function (maxAccel, raceData)
{
	var maxSpeeds = [];
	for (var i=0; i<raceData.race.track.lanes.length; ++i)
	{
		maxSpeeds[i] = [];
		for (var j=0; j<raceData.race.track.pieces.length; ++j)
		{
			if (raceData.race.track.pieces[j].length)
				maxSpeeds[i][j] = -1;
			else
			{
				var laneOffset = raceData.race.track.lanes[i].distanceFromCenter;
				var r = raceData.race.track.pieces[j].radius;
				if (raceData.race.track.pieces[j].angle > 0)
					r += laneOffset;
				else
					r -= laneOffset;
				maxSpeeds[i][j] = Math.sqrt(maxAccel*r);
			}
		}
	}
	return maxSpeeds;
};