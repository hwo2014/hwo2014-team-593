var getPieceLength = require('./getPieceLength.js');

function getSectionLength(i, lane, track)
{
	var sum = 0;
	do {
		sum += getPieceLength(track.lanes[lane].distanceFromCenter, track.pieces[i]);
		++i;
		if (i == track.pieces.length)
			i = 0;
	}while (!track.pieces[i]["switch"]);
	return sum;
}

module.exports = function (i, track)
{
	do {
		++i;
		if (i == track.pieces.length)
			i = 0;
	}while (!track.pieces[i]["switch"]);

	var leftLen = getSectionLength(i,0,track)
	,	rightLen = getSectionLength(i,1,track)
	,	diff = leftLen-rightLen
	;


	return leftLen == rightLen ? 0 : (rightLen < leftLen ? 1 : -1);		// we return:
																		// -1 for left (if right lane is longer)
																		// 0 for no lane change (if both lanes are the same length)
																		// 1 for right (if right lane is shorter)
};