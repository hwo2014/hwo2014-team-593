module.exports = function(laneOffset, piece)
{
	if (piece.length)
		return piece.length;
	else if (piece.radius)
	{
		if (piece.angle > 0)
			return Math.PI * (piece.radius - laneOffset) * 2 * piece.angle / 360;
		else
			return Math.PI * (piece.radius + laneOffset) * 2 * piece.angle / -360;
	}
};