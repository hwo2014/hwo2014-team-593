module.exports = function(speedStart, speedFinal, fraction) {
	if (speedFinal <= 0)
		return;
	var numTicks = 0;
	var speed = speedStart;
	while (speed >= speedFinal) {
		numTicks++;
		speed *= fraction;
	}

	console.log(numTicks);

	return(
	speedStart * ( Math.pow(fraction,numTicks) - 1 )
	/ Math.log(fraction)
	);
};

//2) function that takes 2 speeds and returns the distance it takes to reduce our speed by that much