var getPieceLength = require('./getPieceLength.js');
var	getLaneSwitch = require('./laneSwitches.js');

module.exports = function(piecePos1, piecePos2, track)
{
	var lane = piecePos1.lane.startLaneIndex;
	
	var piece1 = track.pieces[piecePos1.pieceIndex];
	/*if (
		piece1.radius
		&& piecePos1.lane.startLaneIndex != piecePos1.lane.endLaneIndex
	)
	{
		lane = 
			((track.lanes[piecePos1.lane.startLaneIndex].distanceFromCenter > track.lanes[piecePos1.lane.endLaneIndex].distanceFromCenter)
			? (piece1.angle>0)
			: !(piece1.angle>0))
			? piecePos1.lane.startLaneIndex
			: piecePos1.lane.endLaneIndex
		;
	}*/

	if (piecePos1.pieceIndex == piecePos2.pieceIndex)
		return piecePos2.inPieceDistance - piecePos1.inPieceDistance;
	var distance = 
		getPieceLength
		(	track.lanes[lane].distanceFromCenter
		,	track.pieces[piecePos1.pieceIndex]
		)
		-
		piecePos1.inPieceDistance
		+
		piecePos2.inPieceDistance
	;
	
	if (piece1.length && piecePos1.lane.startLaneIndex != piecePos1.lane.endLaneIndex) distance += 2.061245512;//piece1.length / 50;
	if (piece1.radius && piecePos1.lane.startLaneIndex != piecePos1.lane.endLaneIndex) distance += 1.78164075924;//piece1.length / 50;
	
	var i = piecePos1.pieceIndex+1;
	while (true)
	{
		if (i == track.pieces.length)
			i = 0;
		if (i == piecePos2.pieceIndex)
			break;
		distance +=
			getPieceLength
			(	track.lanes[lane].distanceFromCenter
			,	track.pieces[i]
			)
		;
		if (track.pieces[i+1] && track.pieces[i+1]['switch'])
		{
			var newlane = getLaneSwitch(i, track) + lane;
			if (newlane >= 0 && newlane < track.lanes.length)
				lane = newlane;
		}
		++i;
	}
	return distance;
};