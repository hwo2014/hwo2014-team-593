var	getLaneSwitch	= require('./common/laneSwitches.js')
,	getDistance		= require('./common/distanceTo.js')
,	getSpeed 		= require('./common/speedometer.js')
,	calcMaxSpeeds	= require('./common/calcMaxSpeeds.js')
,	distanceToSlow  = require('./common/distanceToSlow.js')

// static variable declaration
,	oldTick = 0
,	oldPos = {}
,	oldThrottle = 0.0
,	oldSpeed = 0
;

var speed;
var car;
var initSpeed;		
var fraction;
var maxSpeeds = [];

//throwaway bullshit variables
var counter = 0;


module.exports = function(color, raceData, data)
{
	var type = data.msgType, tick = data.gameTick, data = data.data;		// makes our messages a little easier to process
	if (type == 'carPositions')
	{
		var msg = {};
		var throttle;
		for (var i=0; i<data.length; ++i)
		{
			if (data[i].id.color === color)
			{
				car = data[i];
				break;
			}
		}
		
		if (tick > 0) {
			speed = getSpeed(oldPos, car.piecePosition, raceData.race.track);
		
			/*** BEGIN FRACTION CALCULATION SECTION ***
			Overview: Fraction calculation means getting the fraction of the speed that is _kept_ between
			consecutive ticks. As it turns out, cars lose a certain fraction of their speed every second.
			For example, a given track may cause a car to lose 1/16th of its speed per second, meaning that
			if it was traveling 16 units per tick (upt) and the throttle is set to 0, it will travel 15
			units in the immediately following tick, meaning that the fraction we are looking for is 15/16.
			Using this fraction, we can precisely measure braking rates and distances for the actual
			competition. To find the fraction, we bring our speed up to an arbitrary point, then set
			throttle=0. The ratio between the speeds of any 2 consecutive speeds is our ratio.
			*/
			
			// accelerate to an arbitrary speed
			if (tick < 10)
			{
				throttle = 1.0;
			}
			
			// stop the engine and record our speed on tick #14
			// multiple ticks are in case the server eats some messages
			// initSpeed is written multiple times, but only the last speed will go through to the next part
			else if (tick < 15)
			{
				throttle = 0.0;
				initSpeed = speed;
			}

			// get fraction by finding the ratio between the speed on tick #14 and #15
			else if (tick < 16)
			{
				throttle = 0.0;
				fraction = speed/initSpeed;			
			}
			// *** END FRACTION CALCULATION SECTION ***
			
			// ALL OF THE SHIT BELOW THIS IS FUCKING WRONG BECAUSE DISTANCETOSLOW GIVES US A COUPLE LESS TICKS THAN IT SHOULD AND THE DISTANCE IS A LITTLE SHORTER THAN IT SHOULD BE
			else if (tick < 35)
			{
				throttle = 0.7;
			}
			else if (tick < 36)
			{
				throttle = 0.0;
				initSpeed = speed;
				console.log("distance:", distanceToSlow(speed, speed-1.65, fraction), "tick:", tick);
				console.log('');
				counter = car.piecePosition;
			}
			else if (tick < 600)
			{
				throttle = 0.0;
				if (speed < initSpeed - 1.65 && counter != "FUCK")
				{
					console.log("distance:", getDistance(counter, car.piecePosition, raceData.race.track), "tick:", tick);
					counter = "FUCK";
				}
			}
		} else
			throttle = 0.0;
//				maxSpeeds = calcMaxSpeeds(maxAccel, raceData);


/*
	3) finish writing the maxAccel
*/
		msg.data = throttle;
		msg.msgType = "throttle";
		oldTick = tick;
		oldPos = car.piecePosition;
		oldSpeed = speed;
		return msg;
	}
	
	if (type == 'crash' && data.color == color)
	{
		var lane = car.piecePosition.lane.endLaneIndex;
		var radius = raceData.race.track.pieces[car.piecePosition.pieceIndex].radius;
	}


	return {msgType: "ping"};
}