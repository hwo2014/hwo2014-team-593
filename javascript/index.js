var	net			= require("net")
,	bot			= require('./bot.js')

,	qualAI		= require('./bots/qualifyingBot.js')
,	quickAI		= require('./bots/qualifyingBot.js')
,	raceAI		= require('./bots/racingBot.js')

,	serverHost	= process.argv[2]
,	serverPort	= process.argv[3]
,	botName		= process.argv[4]
,	botKey		= process.argv[5]
,	serverMsg	=
	{	"msgType":"joinRace"
	,	"data":
		{	"botId":
			{	"name":		botName
			,	"key":		botKey
			}
		,	"trackName":	"keimola"
		}
	}
;

var client = net.connect(serverPort, serverHost, function(){
	serverMsg.data.botId.name = botName;
	client.write(JSON.stringify(serverMsg));
	client.write('\n');
	new bot
	(	botName
	,	qualAI
	,	quickAI
	,	raceAI
	,	client
	);
});
